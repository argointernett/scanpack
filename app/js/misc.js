$(window).trigger('resize').trigger('scroll');

$(document).ready(function(){

  //   if( $('#slideshow').length > 0 && !$('#slideshow').hasClass('static') ) {
	// 	$('#slideshow').argoslide({
	// 		navIconPrev: 'fa fa-angle-left',
	// 		navIconNext: 'fa fa-angle-right'
	// 	});
	// }

	$('a[href="#"').click( function(e) {
		e.preventDefault();
	});

	$('.menutoggle').on('click', function(){
		$('#navTop').toggleClass('open');
	})

	$('.submenuToggle').on('click', function(){
		$('#subMenu').toggleClass('open');
	})

	$('.searchToggle').on('click', function( e )
	{
		e.preventDefault();

		$('#siteSearch').toggleClass('open');
	})

	$('div.fadeIn').hide().delay(500).fadeIn(1000).removeClass('hidden');

	$('a[href$=".pdf"]').addClass("pdf");
	$('a[href$=".doc"]').addClass("doc");
	$('a[href$=".docx"]').addClass("doc");
	$('a[href$=".xls"]').addClass("xls");
	$('a[href$=".xlsx"]').addClass("xls");

	// Scroll
	$(window).scroll(function(){
    	var st = $(this).scrollTop();
		if( st >= 10){
			$('#top').addClass('minimized');
    	}else{
			$('#top').removeClass('minimized');
		}
	});
	// Add this class for when the website loads in the middle of the page
	if( $(window).scrollTop() >= 10)
	{
		$('#top').addClass('minimized');
 	}

	$(window).scroll(function() {
    	$(".scrollOut").css({
			'opacity' : 1-(($(this).scrollTop())/250)
    	});
	});

  //sidebar form
	$('input[name="signnyheter"]').on('change', function(){
		if($(this).is(':checked')){
			$('input[name="bcc"]').val($('form[name="sidebarForm"]').find('input[name="email"]').val())
		}else {
			$('input[name="bcc"]').val('');
		}
	});


	$('form[name="sidebarForm"]').find('input[name="email"]').on('change', function(){
		if($('input[name="signnyheter"]').is(':checked')){
			$('input[name="bcc"]').val($(this).val());
		}else {
			return;
		}
	});

  //contact page form
  $('input[name="signnyheter_contact"]').on('change', function(){
		if($(this).is(':checked')){
			$('input[name="bcc"]').val($('form[name="mainForm"]').find('input[name="email"]').val())
		}else {
			$('input[name="bcc"]').val('');
		}
	});

  $('form[name="mainForm"]').find('input[name="email"]').on('change', function(){
		if($('input[name="signnyheter_contact"]').is(':checked')){
			$('input[name="bcc"]').val($(this).val());
		}else {
			return;
		}
	});


	$('.slickCarousel').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
			breakpoint: 1024,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
    		},
			{
			breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
			breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		]
	});

	// Initierer fancybox om brukeren ikke er på touch skjerm
	if(!$('body').hasClass('touch')) {

		$('.fancybox').fancybox({
			padding: 10,
			scrolling: 'yes',
			nextEffect: 'fade',
			prevEffect: 'fade',
			helpers: {
				overlay: {
				  locked: false
				},
				title: {
					type: 'inside'
				}
			}
		});
	}
  //start mixItUp
  	$('#product-list').mixItUp();


    var thisSlideHeight = 850;
		var thisSlideWidth = 1920;
		var thisRatioSlide = thisSlideHeight / thisSlideWidth;

    //slide height
		$("#slideshow ul.slides>li, #slideshow").css('height', thisRatioSlide * $("#slideshow ul.slides>li, #slideshow").width());
		$(window).resize(function(){
			$("#slideshow ul.slides>li, #slideshow").css('height', thisRatioSlide * $("#slideshow ul.slides>li, #slideshow").width());
      console.log(thisRatioSlide);
		});
    var slideshowDuration = 5000;
    //slider
    $('.flexslider').flexslider({
      animation: "fade",
			slideshowSpeed: slideshowDuration,
			slideshow: true,
      controlsContainer: $(".custom-controls-container"),
      customDirectionNav: $(".custom-navigation a")
    });


});
