$(document).ready(function(e){

	$('#changeCart').on('click', function()
	{
		cart_overlay = $('.cart-overlay');
		btn = $(this);

		var time1 = '';

		if( cart_overlay.is(':visible') )
		{
			return false;
		}
		else
		{
			cart_overlay.fadeIn(300);
			btn.addClass('fa-spin');

			time1 = new Date().getTime();
		}

		var value = $('#changeCart').parents('.cartList').find('input[name=antall]').val();
		var product = $(this).data("product");

		$.ajax
		({
			type: 'POST',
			url: '/@cart/remove',
			data: {
				product: product,
			},
			success: function(response)
			{
				if (value > 0)
				{
					$.ajax
					({
						type: 'POST',
						url: '/@cart/add',
						data: {
							amount: value,
							product: product,
						},
						success: function(response)
						{
							var totalPrice = null;
							var values = [];
							var count = 0;

							$.each(response.cart, function(index, value)
							{
								values = index.split(";");
								totalPrice += (values[3] * value.amount);
								count += value.amount;
							});

							$('.cartSumTotal .price').text( totalPrice +',-' );
							$('.priceCart').text( totalPrice );
							$('.numberCart').text( count++ );

							var time2 = new Date().getTime();

							var diff = time2 - time1;

							if( diff >= 1000 )
							{
								btn.removeClass('fa-spin');
								cart_overlay.fadeOut(300);
							}
							else
							{
								window.setTimeout( function()
								{
									btn.removeClass('fa-spin');
									cart_overlay.fadeOut(300);
								}, diff);
							}
						}
					});
				}
			}
		});
	});

	$('#resetCart').on('click', function(){
		$.ajax({
			type: 'GET',
			url: '/@cart/empty',
			success: function(response){
				window.location.reload(true);
			}
		});
	});

	$("#addCart").on('click', function(e)
	{
		e.preventDefault();
		btn = $(this);

		if( btn.hasClass('disabled') )
		{
			return false;
		}
		else
		{
			btn
				.addClass('disabled')
				.html('<i class="fa fa-list-alt"></i> Laster...</a>');
		}

		$.ajax
		({
			type: 'POST',
			url: '/@cart/add',
			data:
			{
				amount: $("input[name=antall]").val(),
				product: btn.data("id")+';'+btn.data("name")+';'+btn.data("code")+';'+btn.data("price")+';'+btn.data("image"),
			},
			success: function(response)
			{
				getCart();

				btn
					.addClass('alert-box success')
					.html('<i class="fa fa-check"></i> Lagt i forespørselslisten</a>');

				window.setTimeout( function()
				{
					btn
						.removeClass('disabled alert-box success')
						.html('<i class="fa fa-list-alt"></i> Legg i forespørselslisten</a>');
				}, 3000);
			}
		});
	});


	// on load
	getCart();

	function getCart()
	{
		$.ajax
		({
			type: 'GET',
			url: '/@cart/get',
			success: function(response)
			{
				if(response.total > 0)
				{
					$('#headerCart').addClass('open');

					var totalPrice = null;
					var values = [];

					$.each(response.cart, function(index, value)
					{
						values = index.split(";");
						totalPrice += (values[3] * value.amount);
					});

					$('.numberCart').html( response.total );
					$('.priceCart').html( totalPrice );

					$('#headerCart').addClass('open');
				}
			}
		});
	}
});
