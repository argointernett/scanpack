(function( $ ) {

    function resolveCcValue() {
        var val = $('#cc-to-user-toggle').is(':checked') ? $('input[name="email"]').val() : '';
        $('input[name="cc_to"]').val( val );
    }

    $('#cc-to-user-toggle').on( 'change', resolveCcValue );
    $('input[name="email"]').on( 'change', resolveCcValue );

})( jQuery );
