$(document).ready(function(){
    // add "phone" validation method
    $.validator.addMethod( "phone", function( value, element ) {
        return this.optional( element ) || /^\+?[\d ]+$/.test( value );
    });
    
    $('form[name="orderForm"]').each( function() {
        $(this).validate({
            rules: {
                firmanavn: "required",
                kontaktperson: "required",
                telefon: {
                    phone: true,
                    required: true
                },
                epost: {
                    required: true,
                    email: true
                },
                kommentar: "required"
            },
            // Specify validation error messages
            messages: FV_ORDER_MESSAGES,
            submitHandler: function( form ) {
                submitForm.call( form );
            }
        });
    });
    
    $('form[name="mainForm"]').validate({
        rules:{
            name: "required",
            email: {
                required: true,
                email: true
            },
            message: "required",
        },
        
        messages: FV_KONTAKT_MESSAGES,
        submitHandler: function( form ) {
            submitForm.call( form );
        }
    });
    
    $('form[name="sidebarForm"]').validate({
        rules:{
            name: "required",
            email: {
                required: true,
                email: true
            },
            message: "required",
        },
        
        messages: FV_KONTAKT_MESSAGES,
        submitHandler: function( form ) {
            if($(form).find('input[name="signnyheter"]').is(':checked')) {
                $(form).find('input[name="bcc"]').val($(form).find('input[name="email"]').val());
            }
            $.ajax({
                type: 'POST',
                url: '/@communication/mailer/_sidebarFormCaller',
                data: $(form).serialize(),
                success: function(response){
                    if(response.message_sent == true){
                        $('.ctaBox').hide(function(){
                            $('.success-msg').show();
                        });
                    }
                }
            })
        }
    })
});
