$('#subMenu li i').click( function() {
	$li = $(this).parent();
	$ul = $(this).siblings('ul');
	if ($li.hasClass('open')) {
		$ul.slideUp(400);
		$li.removeClass('open');
	} else {
		$ul.slideDown(400);
		$li.addClass('open');
	}
});

$('#subMenu li a.active').each( function(index, el) {
	$(el).siblings('i').click();
});